# Import bibliotheque Tkinter
from tkinter import IntVar,Checkbutton,Button, Text, Label, Tk, END, Menu, PhotoImage, Canvas
from tkinter.messagebox import showinfo, showwarning

# bibliotheque de couleur
color_blue = "#2980b9"
color_lightblue = "#3498db"
color_white = "#ecf0f1"
color_black = "#2c3e50"
color_grey = "#7f8c8d"
color_lightgrey = "#95a5a6"
color_red = "#c0392b"
color_lightred = "#e74c3c"

# Initialisation var chaine
chaine = ''
# Ouverture du fichier Texte et passage en liste
try :
    with open('bad_pass.txt') as bad_pass_txt:
        lines = bad_pass_txt.readlines()
    # Boucle pour enlever les retours chariots
    for line in lines:
        ind =lines.index(line)
        lines[ind] = line.strip()
except IOError:
    showwarning(title="FATAL ERROR", message=" Fichier bad_pass.txt non présent \n ou \n positionnez vous à la racine du fichier .py")


# Récupération du contenue de la zone texte
def get_textarea_content():
    return textarea.get("0.0", END).strip()

# Vérification que la zone texte contienne quelque chose
def check_textarea_content():
    string = get_textarea_content()
    if len(string) < 2 :
        showwarning(title="FATAL ERROR", message="Votre texte doit contenir au moins 2 caractères ")
        return False
    return True

def raz():
    textarea.delete(0.0,END)

# Methode comparer à la liste
def compare():
    verif = 0 
    if check_textarea_content() :
        res=''
        chaine = get_textarea_content()
        chaine =chaine.lower()
# Option cochée ==> vérifie dans tous les mots si le mot est contenue dedans
        if var1.get() ==1 :
            for line in lines :
                if chaine  in line :
                    res= '%s " %s " est le N° %s des mots de passe \n' %(res,line,lines.index(line)+1)
                    verif = 1
# Option non cochée ==> vérifie uniquement le mot
        if var1.get() ==0 :
        # réponse en fonction de la présence ou non
            if chaine in lines :
                res = '" %s " est le N° %s des mots de passe de la liste des 10 000 les plus utilisés '%(chaine, lines.index(chaine)+1) 
                verif = 1       
        # affichage du résulstat
        if verif == 0 :
            res = ' " %s " ne se trouve pas parmis les 10 000 les plus utilisés ' %chaine
        showinfo(title="Resultat",
            message="%s" %(res))


# initialisation de la frame
frame = Tk()
frame.minsize(350,100)
frame.resizable(width=False, height=False)
frame.title("Vérification de mot de passe")
icon = PhotoImage(file='lock.png')
frame.iconphoto(False, icon)

# Insertion d'un logo Via Canvas
logobanner = Canvas(frame,
                        width=650,
                        height=160,
                        bd=0,
                        highlightthickness=0,
                        relief='ridge')
banner = PhotoImage(file='logomdp.png')
logobanner.grid(row=0, columnspan=3, pady=10)
logobanner.create_image(0, 0, image=banner, anchor='nw')

#Insertion indication
label = Label(frame,text='Entrez votre mot de passe à tester')
label.grid(row =1 , column=0)

#Création d'une textarea pour récupérer entrer utilisateur
textarea = Text(frame,
                    wrap='word',
                    bg=color_white,
                    fg=color_black,
                    height=1,
                    width=30)
textarea.insert(END, chaine)
textarea.grid(row=2, column=0, padx=10, pady=10, sticky='nsew')

# Bouton de lancement de la recherche
btn_search = Button(frame,
                             compound="left",
                             text='Verifier',
                             command=compare,
                             bg=color_blue,
                             activebackground=color_lightblue)
btn_search.grid(row=1, column=1, padx= 10, pady=10)

# Bouton RAZ zone de texte
btn_raz = Button(frame,
                             compound="left",
                             text='RAZ',
                             command=raz,
                             bg=color_grey,
                             activebackground=color_lightblue)
btn_raz.grid(row=1, column=0, padx= 10, pady=10, sticky='w')

# Bouton Exit
btn_quit = Button(frame,
                    text="Exit",
                    command=frame.quit,
                    bg=color_red,
                    activebackground=color_lightred)
btn_quit.grid(row=2, column=2)

# CheckBox pour choisir son option
var1= IntVar()
Checkbutton(frame, text="Tous les mdp qui contiennent le terme", variable=var1).grid(row=2,column=1)

# Loop de la frame
frame.mainloop()
